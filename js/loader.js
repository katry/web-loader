export class WebLoader extends HTMLElement {
	connectedCallback() {
		this.classList.add("hidden");
		if(this.svg) {
			fetch(this.svg).then(res => {
				return res.text();
			}).then(txt => {
				this.innerHTML = txt;
			})
		}
		else if(this.img) {
			let img = document.createElement("img");
			img.src = this.img;
			this.appendChild(img);
		}
	}

	show() {
		this.classList.remove("hidden");
	}

	hide() {
		this.classList.add("hidden");
	}

	get svg() {
		return this.getAttribute("svg") || "";
	}

	set svg(s) {
		this.setAttribute("svg", s);
	}

	get img() {
		return this.getAttribute("img") || "";
	}

	set img(s) {
		this.setAttribute("img", s);
	}
}

window.customElements.define('web-loader', WebLoader);
