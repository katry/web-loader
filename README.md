# web-loader

Simple loader web component

## HTML

Use custom tag below in your HTML. Attribute svg contains path to loader animation, you can also use attribute img for gif animation.
```html
<web-loader svg="/img/loader.svg"></web-loader>
```
or
```html
<web-loader img="/img/loader.gif"></web-loader>
```

## JavaScript

Link notification.js script into your HTML.

Get element from DOM and call its show/hide methods.
```js
let loader = document.querySelector("web-loader"); // get loader element
loader.show() // show loader
loader.hide() // hide loader
```

## CSS
Component does not use shadow DOM so that it can be easily styled just by editing CSS.
